##七牛图床

### 环境配置

Python 2.7.11， Tornado 4.3

1. 根目录有一个 requirements.txt 的依赖文件，使用 `pip install -r requirements.txt` 安装依赖
2. 运行 python app.py

### 运行 

    python app.js

### 说明

前端生成 token 的部分 js 来自[http://jsfiddle.net/gh/get/extjs/4.2/icattlecoder/jsfiddle/tree/master/uptoken](http://jsfiddle.net/gh/get/extjs/4.2/icattlecoder/jsfiddle/tree/master/uptoken)

### 支持格式 
URL

> http://cdn.quqqi.com/20160620225324_11B7I4RJ_WEB.png
  
20160620225324 为上传时间戳，11B7I4RJ 为随机生成的字符串， WEB 为固定标识
  
Markdown

> ![](http://cdn.quqqi.com/20160620225324_11B7I4RJ_WEB.png)
> `![](http://cdn.quqqi.com/20160620225324_11B7I4RJ_WEB.png)`



### 相关文档

* [Tornado 官方文档](http://www.tornadoweb.org/en/stable/)
* [七牛开发者文档](http://developer.qiniu.com/code/v6/api/kodo-api/up/upload.html)

